import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service/ApiService';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {
  AlumnoAsistencia,
  estructuraPregunta,
  estructuraPreguntaResponse,
  Examen,
  ExamenResponse,
  RespuestaAlumno
} from '../model/interfaces';
import {any} from 'codelyzer/util/function';


@Component({
  selector: 'app-resolverexamen',
  templateUrl: './resolverexamen.component.html',
  styleUrls: ['./resolverexamen.component.css']
})
export class ResolverexamenComponent implements OnInit {
// tslint:disable-next-line:variable-name
  a;
  b;
  c;
  d;
  e: string;
  mostrar2 = false;
  mostrar3 = false;
  // tslint:disable-next-line:variable-name
  username_Prof: string;
  // tslint:disable-next-line:variable-name
  alumno_User: string;
  nombre: string;
  i: number;
  // tslint:disable-next-line:variable-name
  codigo: string;
  contra: string;
  solution: string;
  mensaje: string;
  mensaje2: string;
  textopregunta: any [] = [{
    texto: null,
    p1: null,
    p2: null,
    p3: null,
    p4: null,
    p5: null
  }] ;
  textorespuesta: any [] = [{
    resp: null,
  }];
  Mostrarexamen: boolean;
  titulo: string;
  resp: string;
  numero: number;
  // tslint:disable-next-line:variable-name
  n_preg: number;
  preguntas: estructuraPreguntaResponse[];
  loginC$: Observable<any>;
  loginA$: Observable<any>;
  constructor(private api: ApiService, private router: Router, private storeLogin: Store<{ loginC: any , loginA: any}>) { }

  ngOnInit(): void {
    this.loginC$ = this.storeLogin.pipe(select('loginC'));
    this.loginC$.subscribe(res => {
      this.username_Prof = res.username_Prof;
      this.nombre = res.nombre;
      this.codigo = res.cod_Examen;
      this.titulo = res.nombre;
    });
    this.loginA$ = this.storeLogin.pipe(select('loginA'));
    this.loginA$.subscribe(res2 => {
      this.alumno_User = res2.username_Al;
    });
  }
  mostrarExamen(): void {
    this.Mostrarexamen = true;
    // @ts-ignore
    const pregunta: Examen = {
      cod_Examen: this.codigo
    };
    this.api.obtenerPreguntas(pregunta).subscribe(data => {
      // @ts-ignore
      this.preguntas = data;
      for (this.i = 0; this.i < this.preguntas.length; this.i++){
        // @ts-ignore
        if (this.codigo === this.preguntas[this.i].cod_Examen){
          const vPregunta: estructuraPregunta = {
            cod_Examen: this.codigo,
            texto: this.preguntas[this.i].texto,
            p1: this.preguntas[this.i].p1,
            p2: this.preguntas[this.i].p2,
            p3: this.preguntas[this.i].p3,
            p4: this.preguntas[this.i].p4,
            p5: this.preguntas[this.i].p5
          };
          console.log(vPregunta);
          this.textopregunta.push(vPregunta);
        }
      }
      if (data != null) {
        this.mensaje = 'visualización de preguntas ';
      }
    });
  }
  empezar(): void {
    this.mostrar2 = true;
  }
  guardarExamen(): void {
    const respuesta: RespuestaAlumno = {
      cod_Examen: this.codigo,
      username_Al: this.alumno_User,
      resp: this.resp,
      n_preg: this.textorespuesta.length ,
    };
    console.log(respuesta.n_preg);
    if (this.resp === 'a' || this.resp === 'b' || this.resp ===  'c' ||this.resp ===  'd' || this.resp === 'e' ){
    this.api.guardarSolucion(respuesta).subscribe(data => {
      this.mensaje2 = '';
      this.textorespuesta.push({
        solucion: this.solution,
      });
      if (this.textorespuesta.length === 11){
        this.mostrar2 = false;
        this.mostrar3 = true;
      }
    });
    this.solution = '';
    }
    else{
      this.mensaje2 = 'Coloque alternativas validas o en minusculas';
    }
  }
  guardarExamen2(): void{
    const Asistencia: AlumnoAsistencia = {
      username_Al: this.alumno_User,
      cod_Examen: this.codigo
    };
    console.log(this.alumno_User);
    console.log(this.codigo);
    // @ts-ignore
    this.api.crearAsistencia(Asistencia).subscribe(dato => {
      this.router.navigateByUrl('');
      if (dato != null){
        this.mensaje2 = 'se guardo el examen correctamente';
      }
    });
  }

}
