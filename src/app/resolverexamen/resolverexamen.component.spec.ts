import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolverexamenComponent } from './resolverexamen.component';

describe('ResolverexamenComponent', () => {
  let component: ResolverexamenComponent;
  let fixture: ComponentFixture<ResolverexamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolverexamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolverexamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
