import {createAction, props} from '@ngrx/store';
import {Alumno, Docente, Examen} from '../model/interfaces';

export const loginA = createAction('loginA',
  props<Alumno>()
);

export const loginD = createAction('loginD',
  props<Docente>()
);

export const loginC = createAction('loginC',
  props<Examen>()
);

