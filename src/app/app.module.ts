import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { RegistroComponent } from './registro/registro.component';
import { AlumnoComponent } from './alumno/alumno.component';
import {StoreModule} from '@ngrx/store';
import {loginReducerA, loginReducerC, loginReducerD} from './reducer/reducer';
import { DocenteComponent } from './docente/docente.component';
import { EditorComponent } from './editor/editor.component';
import { ResolverexamenComponent } from './resolverexamen/resolverexamen.component';
import { RevisionComponent } from './revision/revision.component';
import { GuiaComponent } from './guia/guia.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    InicioComponent,
    NosotrosComponent,
    EncabezadoComponent,
    RegistroComponent,
    AlumnoComponent,
    DocenteComponent,
    EditorComponent,
    ResolverexamenComponent,
    RevisionComponent,
    GuiaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot({loginA: loginReducerA, loginD: loginReducerD, loginC: loginReducerC}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
