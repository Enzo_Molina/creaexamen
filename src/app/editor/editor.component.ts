import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service/ApiService';
import {estructuraPregunta, Examen,TipoExamen} from '../model/interfaces';
import { NgForm } from '@angular/forms';
import { SelectorListContext } from '@angular/compiler';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  a; b; c; d; e: string;
  seleccionado: string;
  titulo: string;
  usernameD: string;
  preguntas: number;
  alternativas: number;
  mostrar: boolean = false;
  mostrar2: boolean = false;
  mostrar3: boolean = false;
  mostrarAg: boolean = true;
  estado: number;
  capturar: string;
  aux: string;
  aux2 = false;
  mensaje: string;
  codigoex: string;
  textopregunta: any [] = [{
    texto: null,
    p1: null,
    p2: null,
    p3: null,
    p4: null,
    p5: null

  }] ;
  textos:string;
  loginD$: Observable<any>;

  tipoPregunta: TipoExamen[] = [
    {id: 1, tipo: 'Seleccione'},
    {id: 2, tipo: 'texto largo'},
    {id: 3, tipo: 'texto corto'},
    {id: 4, tipo: 'alternativa múltiple'}];

  constructor(private api: ApiService, private router: Router, private storeLogin: Store<{ loginD: any }>) { }

  ngOnInit(): void {
    this.loginD$ = this.storeLogin.pipe(select('loginD'));
    this.loginD$.subscribe(res => {
      this.usernameD = res.username_Prof;
    });
  }
  mostrador(): void {
    if (this.seleccionado === 'alternativa múltiple'){
      this.mostrar = true;

    }else{
      this.mostrar = false;
    }
  }

  agregar(): void{
    this.mostrar2 = true;
    this.mostrarAg = false;
  }

  agregarPregunta(): void{
    this.aux2 = true;
    this.aux = this.textos;
    this.textopregunta.push({
      texto: this.textopregunta.length + ') ' + this.textos,
      p1: 'a) ' + this.a,
      p2: 'b) ' + this.b,
      p3: 'c) ' + this.c,
      p4: 'd) ' + this.d,
      p5: 'e) ' + this.e
    });
    if(this.textopregunta.length === 11){
      this.mostrar2 = false;
      this.mostrar3 = true;
      this.mostrar = false;
    };
    const pregunta: estructuraPregunta = {
      cod_Examen: this.codigoex,
      texto: this.textos,
      p1: 'a) ' + this.a,
      p2: 'b) ' + this.b,
      p3: 'c) ' + this.c,
      p4: 'd) ' + this.d,
      p5: 'e) ' + this.e
    };
    this.api.guardarExamen2(pregunta).subscribe(data => {
      if (data != null){
        this.mensaje = 'registro de pregunta satisfactorio';
      }
    });

    this.textos = '',
    this.a = '';
    this.b = '';
    this.c = '';
    this.d = '';
    this.e = '';

  }
  guardarExamen(): void  {
  const Examenpet: Examen = {
    username_Prof: this.usernameD,
    nombre: this.titulo,
    cod_Examen: this.codigoex,
  };
  this.api.crearExamen(Examenpet).subscribe(data => {
    this.router.navigateByUrl('');
});
}
}
