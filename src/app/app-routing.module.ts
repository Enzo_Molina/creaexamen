import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { InicioComponent } from './inicio/inicio.component';
import {RegistroComponent} from './registro/registro.component';
import {AlumnoComponent} from './alumno/alumno.component';
import {DocenteComponent} from './docente/docente.component';
import {EditorComponent} from './editor/editor.component';
import {ResolverexamenComponent} from './resolverexamen/resolverexamen.component';
import {RevisionComponent} from './revision/revision.component';
import {GuiaComponent} from './guia/guia.component';


const routes: Routes = [
  {path: "principal", component:FooterComponent},
  {path: "ayuda", component:NosotrosComponent},
  {path: "inicio", component:EncabezadoComponent},
  {path: "", component:InicioComponent},
  {path: "registro", component:RegistroComponent},
  {path: "alumno", component:AlumnoComponent},
  {path: "docente", component:DocenteComponent},
  {path: "editor", component:EditorComponent},
  {path: "resolver", component:ResolverexamenComponent},
  {path: "revision", component:RevisionComponent},
  {path: "guia", component:GuiaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
