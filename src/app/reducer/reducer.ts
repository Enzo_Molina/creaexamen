import * as states from './../action/action';
import {Alumno, Docente, ExamenResponse} from '../model/interfaces';
import {createReducer, on } from '@ngrx/store';

export const initialAlumno: Alumno = {
  nombre: '',
  apellidomat: '',
  apellidopat: '',
  username_Al: '',
  contrasenia: '',
};


export const initialDocente: Docente = {
  nombre: '',
  apellidomat: '',
  apellidopat: '',
  username_Prof: '',
  contrasenia: '',
};

export const initialCodigo: ExamenResponse = {
  nombre: '',
  cod_Examen: '',
  username_Prof: '',
};

// tslint:disable-next-line:variable-name
const _loginReducerA = createReducer(
  initialAlumno,
  on(states.loginA, (state, {
    nombre, apellidopat, apellidomat, username_Al, contrasenia}) => (
      {nombre: nombre , apellidopat: apellidopat, apellidomat: apellidomat,username_Al: username_Al, contrasenia: contrasenia})),
);

// tslint:disable-next-line:variable-name
const _loginReducerD = createReducer(
  initialDocente,
  on(states.loginD, (state, {
    nombre, apellidopat, apellidomat, username_Prof, contrasenia}) => (
    {nombre:nombre, apellidopat: apellidopat, apellidomat: apellidomat,  username_Prof: username_Prof, contrasenia :contrasenia})),
);

// tslint:disable-next-line:variable-name
const _loginReducerC = createReducer(
  initialCodigo,
  on(states.loginC, (state, {
    nombre, cod_Examen, username_Prof }) => (
    {nombre: nombre, cod_Examen: cod_Examen, username_Prof: username_Prof})),
);

export function loginReducerA(state: Alumno, action) {
  return _loginReducerA(state, action);
}

export function loginReducerD (state: Docente, action) {
  return _loginReducerD(state, action);
}

// tslint:disable-next-line:typedef
export function loginReducerC(state: ExamenResponse, action) {
  return _loginReducerC(state, action);
}
