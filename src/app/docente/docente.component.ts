import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '../service/ApiService';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Alumno, AlumnoAsistencia, Examen} from '../model/interfaces';
import * as action from '../action/action';

@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.css']
})
export class DocenteComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  username_Prof: string;
  nombre: string;
  apellidopat: string;
  apellidomat: string;
  codigoex: string;
  NoExam: boolean;
  i: number;
  iterador: number;
  Examenes: Examen[];
  MostrarExamenes: boolean;
  loginD$: Observable<any>;
  MostrarasistenciaExamen: boolean;
  codigoexAsis: string;
  Asistencia: AlumnoAsistencia[];
  usernamealumno: string;
  codigoexamenrevis: string;
  constructor(private api: ApiService, private router: Router, private storeLogin: Store<{loginA: any, loginD: any, loginC: any}>) {
  }
  ngOnInit(): void {
    this.loginD$ = this.storeLogin.pipe(select('loginD'));
    this.loginD$.subscribe(res => {
      this.nombre = res.nombre;
      this.apellidopat = res.apellidopat;
      this.username_Prof = res.username_Prof;
      this.api.obtenerExamenesDocente(res).subscribe(data => {
        this.Examenes = data;
        if (data != null) {
          this.MostrarExamenes = true;
          console.log(this.Examenes);
          if (data.length === 0) {
            this.NoExam = true;
          }
        }
      });
    });
    this.iterador = 3;
  }

  // tslint:disable-next-line:typedef
  mostrarAsistencias(){
    this.MostrarasistenciaExamen = true;
    const examen2 : Examen = {
      cod_Examen: this.codigoexAsis,
    };
    this.api.obtenerAsistenciaporEx(examen2).subscribe(data => {
        this.Asistencia = data;

    });
  }
  // tslint:disable-next-line:typedef
  RevisarAlumno(){
    const Alumno1: Alumno = {
      username_Al: this.usernamealumno,
    };
    const peticion: Examen = {
      cod_Examen: this.codigoexamenrevis,
    };
    this.api.obtenerExamen(peticion).subscribe( exam => {
      this.storeLogin.dispatch(action.loginC(exam));
    });
    this.api.obtenerAlumnoporUser(Alumno1).subscribe( data => {
      this.storeLogin.dispatch(action.loginA(data));
      this.router.navigateByUrl('/revision');
    });
  }

}
