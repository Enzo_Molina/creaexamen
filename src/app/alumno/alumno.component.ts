import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service/ApiService';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Alumno, AlumnoAsistencia, Examen, ExamenResponse} from '../model/interfaces';
import * as action from './../action/action';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css']
})
export class AlumnoComponent implements OnInit {

  username: string;
  nombre: string;
  apellidopat: string;
  apellidomat: string;
  loginA$: Observable<any>;
  NoExam: boolean;
  i: number;
  codigoex1: string;
  iterador: number;
  Asistencias: AlumnoAsistencia[];
  Examenes: Examen[];
  MostrarExamenes: boolean;
  codigo: string;
  codigos: Examen[];
  errlogC: boolean;
  nombex: string;
  userEx: string;
  rendirExamen: boolean = true ;
  mensaje: string;

  constructor(private api: ApiService, private router: Router, private storeLogin: Store<{ loginA: any }>) {
  }

  ngOnInit(): void {
    this.loginA$ = this.storeLogin.pipe(select('loginA'));
    this.loginA$.subscribe(res => {
      this.nombre = res.nombre;
      this.apellidopat = res.apellidopat;
      this.api.obtenerAsistencia(res).subscribe(data => {
        this.Asistencias = data;
        for (this.i = 0 ; this.i < data.length ; this.i++){
          if(data[this.i].puntaje === 0){
            this.Asistencias[this.i].puntaje = null;
          }
          const Examen: Examen = {
            cod_Examen : data[this.i].cod_Examen ,
          };
          this.api.obtenerExamen(Examen).subscribe(exam => {
            console.log(exam.nombre);
            this.nombex = exam.nombre;
            this.userEx = exam.username_Prof;
            console.log(this.Asistencias[this.i].nombre);
          });
        }
        if (data != null) {
          this.MostrarExamenes = true;
          this.NoExam = false;
          if (data.length === 0) {
            this.NoExam = true;
          }
        }
      });
    });
  }



  // tslint:disable-next-line:typedef
  entrarExamen(){
    console.log(this.codigo);
    const peticion: Examen =  {
      cod_Examen: this.codigo
    };
    console.log(peticion);
    for ( this.i = 0; this.i<this.Asistencias.length ; this.i ++){
      if (this.codigo === this.Asistencias[this.i].cod_Examen){
        this.rendirExamen = false;
        break;
      }
    }
    if (this.rendirExamen){
    this.api.obtenerExamen(peticion).subscribe(examen => {
      console.log(examen);
            const vCodigo: Examen = {
              username_Prof: examen.username_Prof,
              nombre: examen.nombre,
              cod_Examen: this.codigo
            };
            // @ts-ignore
            this.storeLogin.dispatch(action.loginC(vCodigo));
            this.errlogC = false;
            // @ts-ignore
            this.api.entrarExamen(peticion).subscribe(data => {
              this.router.navigateByUrl('/resolver');
            });
        });
    }
  }
}
