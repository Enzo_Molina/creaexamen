import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service/ApiService';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {AlumnoAsistencia, RespuestaAlumno} from '../model/interfaces';

@Component({
  selector: 'app-revision',
  templateUrl: './revision.component.html',
  styleUrls: ['./revision.component.css']
})
export class RevisionComponent implements OnInit {
  nombreAlum: string;
  apellidopat_Alum: string;
  apellidomat_Alum: string;
  usernamealumno:string;
  resp: string;
  n_preg: number;
  puntaje: number;
  loginA$: Observable<any>;
  loginC$: Observable<any>;
  codigoexamen: string;
  i: number;
  filasOn: boolean;
  Respuestas: RespuestaAlumno[];
  mensaje: string;
  constructor(private api: ApiService, private router: Router, private storeLogin: Store<{ loginA: any, loginC: any }>) { }

  ngOnInit(): void {
    this.loginA$ = this.storeLogin.pipe(select('loginA'));
    this.loginC$ = this.storeLogin.pipe(select('loginC'));
    this.loginA$.subscribe(res => {
      this.nombreAlum = res.nombre;
      this.apellidopat_Alum = res.apellidopat;
      this.apellidomat_Alum = res.apellidomat;
      this.usernamealumno = res.username_Al;
      this.loginC$.subscribe(examen => {
        this.codigoexamen = examen.cod_Examen;
      });
      const alumnoasis: AlumnoAsistencia = {
        cod_Examen: this.codigoexamen,
        username_Al: this.usernamealumno,
      };
      console.log(this.codigoexamen);
      this.api.obtenerSoluciondeAlum(alumnoasis).subscribe(sol => {
        this.Respuestas = sol;
        console.log(this.Respuestas);
      });
    });
  }
  // tslint:disable-next-line:typedef
  insertarPuntaje(){
    const peticion: AlumnoAsistencia = {
      username_Al: this.usernamealumno,
      puntaje: this.puntaje,
      cod_Examen: this.codigoexamen,
    };
    console.log(peticion);
    this.api.insertarPuntaje(peticion).subscribe(data =>{
      console.log(data.puntaje);
      this.mensaje = 'Se registro correctamente el puntaje';
      });
  }
}
