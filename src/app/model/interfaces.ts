export interface AlumnoRequest {
  username_Al: string;
  nombre: string;
  apellidopat: string;
  apellidomat: string;
  contrasenia: string;

}

export interface AlumnoResponse {
  username_Al: string;
  nombre: string;
  apellidopat: string;
  apellidomat: string;
  contrasenia: string;

}

export interface DocenteRequest {
  username_Prof: string;
  nombre: string;
  apellidopat: string;
  apellidomat: string;
  contrasenia: string;

}

export interface DocenteResponse {
  username_Prof: string;
  nombre: string;
  apellidopat: string;
  apellidomat: string;
  contrasenia: string;

}

export interface Alumno {
  nombre?: string;
  apellidopat?: string;
  apellidomat?: string;
  username_Al: string;
  contrasenia?: string;
}

export interface Docente {
  nombre?: string;
  apellidopat?: string;
  apellidomat?: string;
  username_Prof: string;
  contrasenia: string;
}

export interface estructuraPregunta{
  cod_Examen: string;
  texto?: string;
  p1?: string; /*?: es para que pueda ser nulo */
  p2?: string;
  p3?: string;
  p4?: string;
  p5?: string;
}

export interface Examen {
  username_Prof?: string;
  nombre?: string;
  cod_Examen: string;

}

export interface ExamenResponse {
  username_Prof?: string;
  nombre?: string;
  cod_Examen: string;
}

export  interface TipoExamen {
  id: number;
  tipo: string;

}

export interface ExamenAlumno {
  cod_Examen?: string;
  username_Al: string;
  puntaje?: number;
}

// tslint:disable-next-line:class-name
export interface estructuraPreguntaResponse {
  cod_Examen: string;
  texto: string;
  p1: string;
  p2: string;
  p3: string;
  p4: string;
  p5: string;
}
export interface RespuestaAlumno {
  cod_Examen: string;
  username_Al: string;
  resp?: string;
  n_preg?: number;
}
export interface AlumnoAsistencia {
  cod_Examen: string;
  username_Al: string;
  nombre?: string;
  username_Prof?: string;
  puntaje?: number;
}

export interface estructuraPreguntatxtc{
  cod_Examen: string;
  textotxt?: string;
}
