import { Component, OnInit } from '@angular/core';
import {Alumno, AlumnoResponse, Docente, DocenteResponse} from '../model/interfaces';
import * as action from '../action/action';
import {ApiService} from '../service/ApiService';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  usernameA: string;
  usernameD: string;
  contraseniaA: string;
  contraseniaD: string;
  i: number;
  errlogA: boolean;
  errlogD: boolean;
  alumnos: AlumnoResponse[];
  docentes: DocenteResponse[];

  constructor(private api: ApiService, private router: Router,  private storeLogin: Store<{loginA: any, loginD: any}>) { }

  ngOnInit(): void {


  }

  loginAlumno () : void{
    const peticion: Alumno = {
      username_Al: this.usernameA,
      contrasenia: this.contraseniaA
    };
    this.api.obtenerAlumnos().subscribe(alum => {
      this.alumnos = alum;
      console.log(alum);
      for (this.i = 0; this.i < this.alumnos.length; this.i++) {
        if (this.usernameA === this.alumnos[this.i].username_Al && this.contraseniaA === this.alumnos[this.i].contrasenia) {
            const vAlumno: Alumno = {
                username_Al: this.usernameA,
                nombre: this.alumnos[this.i].nombre,
                apellidopat: this.alumnos[this.i].apellidopat,
                apellidomat: this.alumnos[this.i].apellidomat,
                contrasenia: this.contraseniaA
            };
            this.storeLogin.dispatch(action.loginA(vAlumno));
            console.log(vAlumno);
            this.errlogA = false;
            this.api.loginAlumno(peticion).subscribe(data => {
                  this.router.navigateByUrl('/alumno');
              });
        }
        else {
            this.errlogA = true;
        }
      }

      }
     );


  }

  loginDocente () : void{
    const peticion: Docente = {
      username_Prof: this.usernameD,
      contrasenia: this.contraseniaD
    };
    this.api.obtenerDocentes().subscribe(docen => {
        this.docentes = docen;
        console.log(docen);
        for (this.i = 0; this.i < this.docentes.length; this.i++) {
          if (this.usernameD === this.docentes[this.i].username_Prof && this.contraseniaD === this.docentes[this.i].contrasenia) {
            const vDocente: Docente = {
              username_Prof: this.usernameD,
              nombre: this.docentes[this.i].nombre,
              apellidopat: this.docentes[this.i].apellidopat,
              apellidomat: this.docentes[this.i].apellidomat,
              contrasenia: this.contraseniaD
            };
            this.storeLogin.dispatch(action.loginD(vDocente));
            this.errlogD = false;
            this.api.loginDocente(peticion).subscribe(data => {
              this.router.navigateByUrl('/docente');
            });
          }
          else {
            this.errlogD = true;
          }
        }

      }
    );


  }

}
