import { Component, OnInit } from '@angular/core';
import {ApiService} from '../service/ApiService';
import {AlumnoRequest, AlumnoResponse, DocenteRequest, DocenteResponse} from '../model/interfaces';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  usernameA: string;
  usernameD: string;
  nombreA: string;
  nombreD: string;
  apellidoApat: string;
  apellidoAmat: string;
  apellidoDpat: string;
  apellidoDmat: string;
  contraseniaD: string;
  contraseniaA: string;
  VerifRegistro: boolean = true;
  Exito: string;
  Registroerr: string;

  i: number;
  alumnos: AlumnoResponse[];
  docentes: DocenteResponse[];
  errorRegA: boolean;
  errorRegD: boolean;
  constructor(private api: ApiService) {
  }


  ngOnInit(): void {

  }

  // tslint:disable-next-line:typedef
  RegistrarAlumno() {
    const peticion: AlumnoRequest = {
      username_Al: this.usernameA,
      nombre: this.nombreA,
      apellidopat: this.apellidoApat,
      apellidomat: this.apellidoAmat,
      contrasenia: this.contraseniaA,
    };
    this.api.obtenerAlumnos().subscribe(alum => {
      this.alumnos = alum;
      console.log(alum);
      for (this.i = 0; this.i < this.alumnos.length; this.i++) {
        if (this.usernameA === this.alumnos[this.i].username_Al) {
          this.VerifRegistro = false;
          break;
        }
        if (this.alumnos.length === 0 ){
          this.VerifRegistro = true;
        }
      }
      if (this.VerifRegistro === true && this.usernameA.length < 11 ) {
        this.api.registrarAlumno(peticion).subscribe(data => {
          console.log('usuario creado');
          this.Exito = 'Registro exitoso';
          this.errorRegA = false;

        });
      }
      else {
        this.errorRegA = true;
      }
      if (this.errorRegA){
        if (this.usernameA.length < 10){
          this.Registroerr = 'Nombre de usuario ya en uso';
        }
        else {
          this.Registroerr = 'Debe contener max. 10 caracteres';
        }
      }
    } );
  }

  // tslint:disable-next-line:typedef
  RegistrarDocente(): void {
    const peticion: DocenteRequest = {
      username_Prof: this.usernameD,
      nombre: this.nombreD,
      apellidopat: this.apellidoDpat,
      apellidomat: this.apellidoDmat,
      contrasenia: this.contraseniaD,
    };
    this.api.obtenerDocentes().subscribe(docen => {
      this.docentes = docen;
      console.log(docen);
      for (this.i = 0; this.i < this.docentes.length; this.i++) {
        if (this.usernameD === this.docentes[this.i].username_Prof) {
          this.VerifRegistro = false;
          break;
        }
      }
      if (this.docentes.length === 0){
        this.VerifRegistro = true;
      }
      if (this.VerifRegistro === true && this.usernameD.length < 11 ) {
        this.api.registrarDocente(peticion).subscribe(data => {
          console.log('usuario creado');
          this.Exito = 'Registro exitoso';
          this.errorRegD = false;
        });
      }
      else {
        this.errorRegD = true;
      }
      if (this.errorRegD){
        if (this.usernameD.length < 11){
          this.Registroerr = 'Nombre de usuario ya en uso';
        }
        else {
          this.Registroerr = 'Debe contener max. 10 caracteres';
        }
      }
    } );
  }
}
