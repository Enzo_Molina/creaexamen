import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import {
  AlumnoRequest,
  AlumnoResponse,
  DocenteResponse,
  DocenteRequest,
  Alumno,
  Docente,
  estructuraPregunta,
  Examen, ExamenAlumno, RespuestaAlumno, AlumnoAsistencia, ExamenResponse
} from '../Model/interfaces';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json;charset=utf-8'
    })
  };

  // tslint:disable-next-line:typedef
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  constructor(private http: HttpClient) {
  }


  registrarAlumno(data: AlumnoRequest): Observable<AlumnoResponse> {
    return this.http.post<AlumnoResponse>('http://localhost:8080/registroAlumno', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  registrarDocente(data: DocenteRequest): Observable<DocenteResponse> {
    return this.http.post<DocenteResponse>('http://localhost:8080/registroDocente', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  obtenerAlumnos(): Observable<AlumnoResponse[]> {
    return this.http.post<AlumnoResponse[]>('http://localhost:8080/obtenerAlumnos', this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  obtenerDocentes(): Observable<DocenteResponse[]> {
    return this.http.post<DocenteResponse[]>('http://localhost:8080/obtenerDocentes', this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  loginAlumno(data: Alumno): Observable<AlumnoResponse> {
    return this.http.post<AlumnoResponse>('http://localhost:8080/loginAlumno', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  loginDocente(data: Docente): Observable<DocenteResponse> {
    return this.http.post<DocenteResponse>('http://localhost:8080/loginDocente', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  guardarExamen2(data: estructuraPregunta): Observable<estructuraPregunta> {
    return this.http.post<estructuraPregunta>('http://localhost:8080/ingresarPregunta', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));

  }

  crearExamen(data: Examen): Observable<Examen> {
    return this.http.post<Examen>('http://localhost:8080/crearExamen', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));

  }

  obtenerExamenesDocente(data: Docente): Observable<Examen[]> {
    return this.http.post<Examen[]>('http://localhost:8080/obtenerExamenporDocente', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));

  }

  obtenerExamenesAlumno(data: Alumno): Observable<ExamenAlumno[]> {
    return this.http.post<ExamenAlumno[]>('http://localhost:8080/obtenerExamenAlumno', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));

  }

  entrarExamen(data: ExamenResponse): Observable<Examen> {
    return this.http.post<Examen>('http://localhost:8080/entrarExamen', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  obtenerPreguntas(data: Examen): Observable<estructuraPregunta[]> {
    return this.http.post<estructuraPregunta[]>('http://localhost:8080/responderExamen', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  obtenerExamen(data: Examen): Observable<Examen> {
    return this.http.post<Examen>('http://localhost:8080/obtenerExamenPorCod', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  guardarSolucion(data: RespuestaAlumno): Observable<RespuestaAlumno> {
    return this.http.post<RespuestaAlumno>('http://localhost:8080/ingresarRespuesta', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));

  }

  crearAsistencia(data: AlumnoAsistencia): Observable<AlumnoAsistencia> {
    return this.http.post<AlumnoAsistencia>('http://localhost:8080/crearAsistencia', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  obtenerAsistencia(data: Alumno): Observable<AlumnoAsistencia[]> {
    return this.http.post<AlumnoAsistencia[]>('http://localhost:8080/obtenerAsistencia', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  obtenerAsistenciaporEx(data: Examen): Observable<AlumnoAsistencia[]> {
    return this.http.post<AlumnoAsistencia[]>('http://localhost:8080/obtenerAsistenciaporEx', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  obtenerSoluciondeAlum(data: AlumnoAsistencia): Observable<RespuestaAlumno[]> {
    return this.http.post<RespuestaAlumno[]>('http://localhost:8080/obtenerSolucionAlumno', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  obtenerAlumnoporUser(data: Alumno): Observable<Alumno> {
    return this.http.post<Alumno>('http://localhost:8080/obtenerAlumnosporUser', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

  insertarPuntaje(data: AlumnoAsistencia): Observable<AlumnoAsistencia>{
    return this.http.post<AlumnoAsistencia>('http://localhost:8080/insertarPuntaje', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl));
  }

}
